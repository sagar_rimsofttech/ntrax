        <!-- Vendor js -->
        <script src="{{ URL::asset('assets/js/vendor.min.js')}}"></script>

        @yield('script')
        <script src="{{ URL::asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/switchery/switchery.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/multiselect/multiselect.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
        {{-- <script src="{{ URL::asset('assets/libs/jquery-mockjax/jquery-mockjax.min.js')}}"></script> --}}
        <script src="{{ URL::asset('assets/libs/autocomplete/autocomplete.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/toastr.js')}}"></script>
        <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
        <!-- Init js-->
        <script src="{{ URL::asset('assets/js/pages/form-advanced.init.js')}}"></script>
        <!-- App js -->
        <script src="{{ URL::asset('assets/js/app.min.js')}}"></script>
        <script>
                $(document).ready(function () {
                @if($errors->any())
                    @foreach($errors->all() as $error)
                    toastr.error('{{ $error }}','Error',{
                          closeButton:true,
                          progressBar:true,
                       });
                    @endforeach
                @endif
                });
          </script>
          <script type="text/javascript">
            function isNumber(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if ( (charCode > 31 && charCode < 48) || charCode > 57) {
                        return false;
                    }
                    return true;
                }

        function isFloat(event) {
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    return false;
                }
                return true;
        }
            </script>
           @if(Session::has('message'))
           <script>
                $(document).ready(function () {
                   var type = "{{ Session::get('alert-type', 'info') }}";
                   switch(type){
                       case 'info':
                           toastr.info("{{ Session::get('message') }}");
                           break;
       
                       case 'warning':
                           toastr.warning("{{ Session::get('message') }}");
                           break;
       
                       case 'success':
                           toastr.success("{{ Session::get('message') }}");
                           break;
       
                       case 'error':
                           toastr.error("{{ Session::get('message') }}");
                           break;
                   }
           });
           </script>
           @endif 
        @yield('script-bottom')