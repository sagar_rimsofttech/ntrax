@extends('layouts.master')

@section('css')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
        <link href="{{ URL::asset('assets/libs/custombox/custombox.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
        <title>Ntrax - Prospect</title>
        <link href="{{ URL::asset('assets/libs/jquery-toast/jquery-toast.min.css')}}" rel="stylesheet" type="text/css" />
        <style>
            .floatRight {
            float: right;
            margin-left:10px; 
            }
        </style>

@endsection

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Masters</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Prospect</a></li>
                                            {{-- <li class="breadcrumb-item active">Datatables</li> --}}
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Add Prospect</h4>
                                </div>
                            </div>
                        </div>     
                        {{-- {{
                            dd($prospectdetail,$prospectproductdetail)
                        }} --}}
                        <!-- end page title --> 
                        <div class="row">
                            <div class="colg-lg-12">
                            <form class="form" action="{{route('prospect.updateprospect',$prospectdetail->id)}}" method="post">
                                {{csrf_field()}}
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                Select the Segment: &nbsp;
                                                <div class="radio radio-info form-check-inline">
                                                    <input type="radio" id="inlineRadio1" value="B2B" name="business" @if($prospectdetail->segment =="B2B") checked @endif>
                                                    <label for="inlineRadio1">  B2B </label>
                                                </div>
                                                <div class="radio radio-info form-check-inline">
                                                    <input type="radio" id="inlineRadio2" value="B2C" name="business" @if($prospectdetail->segment =="B2C") checked @endif>
                                                    <label for="inlineRadio2"> B2C </label>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="checkbox checkbox-info mt-1 float-right">
                                                    <select class="form-control "  data-placeholder="Choose Channel Type..." id="channelpartnertype" name="channelpartnertype" style="width:100%" data-style="btn-outline-primary" @if ($prospectdetail->channel_partner_id==0)disabled @endif>
                                                        <option value="">Select Channel partner</option>
                                                        @foreach ($channelpartnertypes as $value)
                                                    <option value="{{$value->id}}" @if($prospectdetail->channel_partner_id ==$value->id) selected @endif >{{$value->name}}</option>
                                                        @endforeach
                                                       
                                                    </select>
                                                </div> 
                                            <div class="mb-2 float-right">
                                                <div class="checkbox checkbox-info mt-0">
                                                    <input id="checkbox4" type="checkbox" @if ($prospectdetail->channel_partner_id!=0)checked @endif>
                                                    <label for="checkbox4">
                                                        
                                                    </label>
                                                </div>
                                            </div>
                                        </div> <!-- end card -->
                                    </div><!-- end col-->
                                    <hr>
                                    <div class="row">&nbsp;</div>
                                    <div class="col-lg-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                               <label for="fname">First Name</label>
                                            <input type="text" class="form-control" value="{{$prospectdetail->first_name??''}}" name="fname" id="fname">
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="lname">Last Name</label>
                                                <input type="text" class="form-control" name="lname" id="lname" value="{{$prospectdetail->last_name??''}}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="phone">Phone Number</label>
                                                <input type="text" class="form-control" name="phone" id="phone" value="{{$prospectdetail->phone??''}}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control" name="email" id="email" value="{{$prospectdetail->email??''}}">
                                             </div>
                                    </div><!-- end col-->
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                               <label for="source">Source</label>
                                               <select class="form-control" name="source" id="source">
                                                <option value="">Select Source</option>
                                                @foreach(config('constant.source') as $key => $value)
                                            <option value="{{$key}}" @if($prospectdetail->source ==$key) selected @endif>  {{$value}} </option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="customertype">Customer Type</label>
                                                <select class="form-control" name="customertype" id="customertype">
                                                    <option value="">Select Customer Type</option>
                                                    @foreach(config('constant.customer_type') as $key => $value)
                                                <option value="{{$key}}" @if($prospectdetail->customer_type ==$key) selected @endif>  {{$value}} </option>
                                                    @endforeach
                                                    </select>
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="addphone">Company Name</label>
                                                <input type="text" class="form-control" name="company_name" id="company_name" value="{{$prospectdetail->company_name??''}}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="addphone">SPOC Name</label>
                                                <input type="text" class="form-control" name="spocname" id="spocname" value="{{$prospectdetail->spoc_name??''}}">
                                             </div>
                                    </div><!-- end col-->
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                               <label for="source">SPOC Email</label>
                                               <input type="text" class="form-control" name="spocemail" id="spocemail" value="{{$prospectdetail->spoc_email??''}}">
                                            </div>
                                           
                                             <div class="form-group col-md-3">
                                                <label for="addphone">Additional Phone Number</label>
                                                <input type="text" class="form-control" name="addphone" id="addphone" value="{{$prospectdetail->addit_phone??''}}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="country">Country</label>
                                                <select id="country_name" class="form-control country-select2" title="" name="country_name" >
                        
                                            </select>
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="state">State</label>
                                                <select id="state_name" class="form-control state-select2"
                                                data-show-subtext="true" name="state_name" data-live-search="true" title="">
                        
                                            </select>
                                            </div>
                                    </div><!-- end col-->
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-row">
                                            
                                            <div class="form-group col-md-3">
                                                <label for="city">City</label>
                                                <select id="city_name" class="form-control city-select2"
                                                data-show-subtext="true" name="city_name" data-live-search="true" title="">
                        
                                            </select>
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="zone">Zone</label>
                                                <select class="form-control "  data-placeholder="Choose Zone..." id="zone" name="zone" style="width:100%" data-style="btn-outline-primary">
                                                    <option value="">Select Zone</option>
                                                    @foreach ($zones as $value)
                                                <option value="{{$value->id}}" @if($prospectdetail->zone ==$value->id) selected @endif>{{$value->zonename}}</option>
                                                    @endforeach
                                                   
                                                </select>
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="address">Address</label>
                                                <input type="text" class="form-control" name="address" id="address" value="{{$prospectdetail->address??''}}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="pincode">Pincode</label>
                                                <input type="text" class="form-control" name="pincode" id="pincode" value="{{$prospectdetail->pincode??''}}" maxlength="6">
                                             </div>
                                    </div><!-- end col-->
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-row">
                                           
                                            
                                             <div class="form-group col-md-3">
                                                <label for="googleaddress">Google Address</label>
                                                <input type="text" class="form-control" name="googleaddress" id="searchTextField" value="{{$prospectdetail->google_address??''}}">
                                             </div>
                                    </div><!-- end col-->
                                    </div>
                                    <hr>
                                    <div class="row">&nbsp;</div>
                                    <div class="col-lg-12">
                                        <table class="table  table order-list">
                                            <thead>
                                                <tr>
                                                <th>Product</th>
                                                <th>SubProduct</th>
                                                <th>SubsubProduct</th>
                                                <th>Price</th>
                                                <th>Margin</th>
                                                <th>Discount</th>
                                                <th>Total Price</th>
                                                <th>Tax</th>
                                                <th>Total Net Price</th>
                                                <th><button  id="addrow" class="btn btn-primary"><i class="fas fa-plus"></i></button> </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                 $id=0;   
                                                @endphp
                                                {{-- @if(count($prospectproductdetail)>0) --}}
                                                @foreach ($prospectproductdetail as $key => $value)
                                                {{-- @endif --}}
                                                 @php
                                                 
                                                 $id++;  
                                                @endphp

                                                <tr class="appenddata">
                                                    <td>
                                                        <div class="form-group">
                                                        <select name="product[]" class="form-control product" id="product{{$id}}" style="width: 127px;">
            
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="subproduct[]" class="form-control select2 subproduct" id="subproduct{{$id}}" style="width: 127px;">
            
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="subsubproduct[]" class="form-control select2 subsubproduct" id="subsubproduct{{$id}}" style="width: 127px;">
            
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                            <input type="text" name="price[]" id="price{{$id}}" class="form-control onlynumbers price" readonly onfocus="this.blur()" onkeypress="return isNumber(event)" value="{{$value->price??''}}">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="margin[]" id="margin{{$id}}" class="form-control margin" readonly onfocus="this.blur()" onkeypress="return isNumber(event)" value="{{$value->margin??''}}" >
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="number" name="discount[]" id="discount{{$id}}" class="form-control discount" readonly onfocus="this.blur()" value="{{$value->discount??''}}">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="totalprice[]" id="totalprice{{$id}}" class="form-control totalprice" value="{{$value->total_price??''}}">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="tax[]" id="tax{{$id}}" class="form-control tax" value="{{$value->tax??''}}" >
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="totalnetprice[]" id="totalnetprice{{$id}}" class="form-control totalnetprice" value="{{$value->total_net_price??''}}">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><button  class="ibtnDel btn btn-primary"><i class="fas fa-minus"></i></button></td>
                                                </tr>
                                                {{-- @if(count($prospectproductdetail)>0) --}}
                                                @endforeach
                                              
                                        </tbody>
                                        </table>
                                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                        <button type="submit" class="btn btn-success">Update</button>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div> <!-- container -->
                    </form>
                </div>
                        </div>
                    </div>
                </div>
               
@endsection

@section('script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-XeQiy32hVZnA-Oy9db3bhHRvLpKi3Cc&libraries=places"></script>
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <!-- third party js -->
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <!-- third party js ends -->
        <script src="{{ URL::asset('assets/libs/custombox/custombox.min.js')}}"></script>
        <!-- Datatables init -->
        <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>
       
 <script src="{{ URL::asset('assets/libs/custombox/custombox.min.js')}}"></script>
 <script>
$(document).ready(function () {
        @php $id=0; @endphp  
        @foreach($prospectproductdetail as $key =>$value)
        @php $id++; @endphp 
        var id1={{$id}};
        var product = "{{$value->product->name?$value->product->name:''}}";
        var subproduct = "{{$value->subproduct->name?$value->subproduct->name:''}}";
        var subsubproduct = "{{$value->subsubproduct->name?$value->subsubproduct->name:''}}";
        var product_id = "{{$value->product->id?$value->product->id:''}}";
        var subproduct_id = "{{$value->subproduct->id?$value->subproduct->id:''}}";
        var subsubproduct_id = "{{$value->subsubproduct->id?$value->subsubproduct->id:''}}";
        
        $('#product'+id1).append("<option value="+product_id+" selected>"+product+"</option>");
        $('#subproduct'+id1).append("<option value="+subproduct_id+" selected>"+subproduct+"</option>");
        $('#subsubproduct'+id1).append("<option value="+subsubproduct_id+" selected>"+subsubproduct+"</option>");
        @endforeach

        $('input[type="checkbox"]').click(function() { 
            
    if ($(this).is(':checked')) {
        //$(this).prop('checked',false);
        $("#channelpartnertype").removeAttr("disabled");
    } else {
         //$(this).prop('checked',true);
         $("#channelpartnertype").attr("disabled", "disabled"); 
    }
});
        
     })
 </script>

 <script>
      $(document).ready(function () {
        var country = "{{$prospectdetail->country}}";
        var country_name = "{{$prospectdetail->country_name}}";
        var state = "{{$prospectdetail->state}}";
        var state_name = "{{$prospectdetail->state_name}}";
        var city = "{{$prospectdetail->city}}";
        var city_name = "{{$prospectdetail->city_name}}";
        console.log(country+","+country_name+","+state+","+state_name+","+city+","+city_name);
        $('.country-select2').append("<option value="+country+" selected>"+country_name+"</option>").trigger('change');
        $('.state-select2').append("<option value="+state+" selected>"+state_name+"</option>").trigger('change');
        $('.city-select2').append("<option value="+city+" selected>"+city_name+"</option>").trigger('change');
     })
     $(document).ready(function () {
        function initialize() {
        var input = document.getElementById('searchTextField');
        new google.maps.places.Autocomplete(input);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
      $(".country-select2").select2({
        ajax: {
            url: function (params) {
                return "{{route('prospect-country').'/'}}" + params.term;
            },
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {q: params.term, };
            },
            processResults: function (data, params) {
                return {results: data};
            },
            cache: true
        },
        minimumInputLength: 3,
    });


    $( ".country-select2" ) .change(function () {
        $("#state_name").val('');
        $("#city_name").val('');
        let param_new = $(this).val();
        var param_country = "?country_id="+param_new;
        $(".state-select2").select2({
        ajax: {
            url: function (param) {
                return "{{route('prospect-state').'/'}}" + param.term + param_country;
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        // minimumInputLength: 3,
    });
    });

    $( ".state-select2" ) .change(function () {
        $("#city_name").val('');
        let param_new = $(this).val();
        var param_state = "?state_id="+param_new;
        $(".city-select2").select2({
        ajax: {
            url: function (param) {
                return "{{route('prospect-city').'/'}}" + param.term + param_state;
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        // minimumInputLength: 3,
    });
    });
    })
     $(document).ready(function () {
        $(".product").select2({
        ajax: {
            url: function (param) {
                return "{{route('search.product').'/'}}"+ params.term;
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
              console.log(param);
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        minimumInputLength: 3,
    });
    $( ".product" ) .change(function () {
        $("#subproduct").val('');
        let param_new = $(this).val();
        var param_product = "?product_id="+param_new;
        $(".subproduct").select2({
        ajax: {
            url: function (param) {
                return "{{route('search.subproduct').'/'}}" + param.term + param_product;
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        // minimumInputLength: 3,
    });

    });
    $( ".subproduct" ) .change(function () {
        $("#subsubproduct").val('');
        let param_new = $(this).val();
        var param_subproduct = "?subproduct_id="+param_new;
        $(".subsubproduct").select2({
        ajax: {
            url: function (param) {
                return "{{route('search.subsubproduct').'/'}}" + param.term + param_subproduct;
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        // minimumInputLength: 3,
    });
    });
    $(".subproduct").select2({
        ajax: {
            url: function (param) {
                return "{{route('search.subproduct')}}";
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
              console.log(param);
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        minimumInputLength: 3,
    });
    $(".subsubproduct").select2({
        ajax: {
            url: function (param) {
                return "{{route('search.subsubproduct')}}";
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
              console.log(param);
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        minimumInputLength: 3,
    });

    $(".subsubproduct").change(function () {
       
        let subsubproduct_name = $(this).val();
       var parenttr=$(this).parents('.appenddata');
        $.ajax({
            url:"/admin/subsubproduct/"+subsubproduct_name+"/edit",
            dataType:"json",
            success:function(html){
               parenttr.find('.price').val(html.data.rate);
               parenttr.find('.margin').val(html.data.margin);
               parenttr.find('.discount').val(html.data.max_discount);
               var product_rate=html.data.rate;
               var product_discount=html.data.max_discount;
              
                var producttotalprice="";
               if(isNaN(product_rate) || isNaN(product_discount)){
                producttotalprice="0";
                 }else{
                        producttotalprice = parseFloat(product_rate)-(parseFloat(product_rate)*(parseFloat(product_discount)/100));
                }
                parenttr.find('.totalprice').val(producttotalprice);
            }
            })

    });
    $('.tax').on('keyup change',function(){
        var totalprice = $(this).closest('tr').find("td input[name^='totalprice']").val();
        var tax = $(this).closest('tr').find("td input[name^='tax']").val();
        var totalnetprice="";
        if(isNaN(totalprice) || isNaN(tax)){
            totalnetprice="0";
        }
        else{
            totalnetprice = parseFloat(totalprice)+(parseFloat(totalprice)*(parseFloat(tax)/100));
        }
        $(this).closest('tr').find("td input[name^='totalnetprice']").val(totalnetprice);
        $('.addrow').show();
    });
     var counter = 0;

$("#addrow").on("click", function (e) {
    e.preventDefault()

    var newRow = $("<tr class='appenddata'>");
    var cols = "";
    var col =[];

    cols += '<td><div class="form-group"><div class="input-group"><select  class="form-control select2 product" id="product" name="product[]" style="width: 127px;"></select></div></div></td>';
    cols += '<td><div class="form-group"><div class="input-group"><select  class="form-control select2 subproduct" id="subproduct" name="subproduct[]" style="width: 127px;"></select></div></div></td>';
    cols += '<td><div class="form-group"><div class="input-group"><select  class="form-control select2 subsubproduct" id="subsubproduct" name="subsubproduct[]" style="width: 127px;"></select></div></div></td>';
    cols += '<td><div class="form-group"><div class="input-group price"><input type="number" class="form-control price" readonly onfocus="this.blur()" name="price[]"/></div></div></td>';
    cols += '<td><div class="form-group"><div class="input-group margin"><input type="number" readonly onfocus="this.blur()" class="form-control margin" name="margin[]"/></div></div></td>';
    cols += '<td><div class="form-group"><div class="input-group discount"><input type="number" readonly onfocus="this.blur()" class="form-control discount" name="discount[]"/></div></div></td>';
    cols += '<td><div class="form-group"><div class="input-group"><input type="text" class="form-control totalprice" name="totalprice[]"/></div></div></td>';
    cols += '<td><div class="form-group"><div class="input-group"><input type="text" class="form-control tax" name="tax[]"/></div></div></td>';
    cols += '<td><div class="form-group"><div class="input-group"><input type="text" class="form-control totalnetprice" name="totalnetprice[]"/></div></div></td>';

    cols += '<td><button  class="ibtnDel btn btn-primary"><i class="fas fa-minus"></i></button></td>';
    newRow.append(cols);
    newRow.find(".product").select2({
    ajax: {
        url: function (param) {
            return "{{route('search.product')}}";
        },
        dataType: 'json',
        delay: 250,
        data: function (param) {
            return {q: param.term, };
        },
        processResults: function (data, param) {
            return {results: data};
        },
        cache: true
    },
    minimumInputLength: 3,
});
newRow.find( ".product" ) .change(function () {
    newRow.find("#subproduct").val('');
        let param_new = $(this).val();
        var param_product = "?product_id="+param_new;
        newRow.find(".subproduct").select2({
        ajax: {
            url: function (param) {
                return "{{route('search.subproduct').'/'}}" + param.term + param_product;
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        // minimumInputLength: 3,
    });

    });
    newRow.find( ".subproduct" ) .change(function () {
        newRow.find("#subsubproduct").val('');
        let param_new = $(this).val();
        var param_subproduct = "?subproduct_id="+param_new;
        newRow.find(".subsubproduct").select2({
        ajax: {
            url: function (param) {
                return "{{route('search.subsubproduct').'/'}}" + param.term + param_subproduct;
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        // minimumInputLength: 3,
    });
    });
newRow.find(".subproduct").select2({
    ajax: {
        url: function (param) {
            return "{{route('search.subproduct')}}";
        },
        dataType: 'json',
        delay: 250,
        data: function (param) {
            return {q: param.term, };
        },
        processResults: function (data, param) {
            return {results: data};
        },
        cache: true
    },
    minimumInputLength: 3,
});
newRow.find(".subsubproduct").select2({
    ajax: {
        url: function (param) {
            return "{{route('search.subsubproduct')}}";
        },
        dataType: 'json',
        delay: 250,
        data: function (param) {
            return {q: param.term, };
        },
        processResults: function (data, param) {
            return {results: data};
        },
        cache: true
    },
    minimumInputLength: 3,
});
newRow.find('.quantity,.price').on('keyup change',function(){
    var price = $(this).closest('tr').find("td input[name^='price']").val();
    var quantity = $(this).closest('tr').find("td input[name^='quantity']").val();
    var tot = price * quantity;
    $(this).closest('tr').find("td input[name^='amount']").val(tot);
});
// newRow.on('change', '.product', function (e) {
//         var val = $(this).val();
//         var selects = $('.product').not(this);
//         for (var index = 0; index < selects.length; index++) {
//             if (val === $(selects[index]).val()) {
//                 toastr.error("This Product is already added.");
//                 $(this).val("").trigger('change');
//                 break;
//             }
//         }
//     });
    newRow.on('change', '.subsubproduct', function (e) {
        let subsubproduct_name = $(this).val();
        var parenttr=$(this).parents('.appenddata');
        $.ajax({
            url:"/admin/subsubproduct/"+subsubproduct_name+"/edit",
            dataType:"json",
            success:function(html){
               parenttr.find('.price').val(html.data.rate);
               parenttr.find('.margin').val(html.data.margin);
               parenttr.find('.discount').val(html.data.max_discount);
               var product_rate=html.data.rate;
               var product_discount=html.data.max_discount;
                var producttotalprice="";
               if(isNaN(product_rate) || isNaN(product_discount)){
                producttotalprice="0";
                 }else{
                        producttotalprice = parseFloat(product_rate)-(parseFloat(product_rate)*(parseFloat(product_discount)/100));
                }
                parenttr.find('.totalprice').val(producttotalprice);
            }
            })

     });
     newRow.on('keyup change', '.tax', function (e) {
        var totalprice = $(this).closest('tr').find("td input[name^='totalprice']").val();
        var tax = $(this).closest('tr').find("td input[name^='tax']").val();
        var totalnetprice="";
        if(isNaN(totalprice) || isNaN(tax)){
            totalnetprice="0";
        }
        else{
            totalnetprice = parseFloat(totalprice)+(parseFloat(totalprice)*(parseFloat(tax)/100));
        }
        $(this).closest('tr').find("td input[name^='totalnetprice']").val(totalnetprice);

    });

    $("table.order-list").append(newRow);
    //     $("table.order-list").find(".flat-red").iCheck({
    //   checkboxClass: 'icheckbox_flat-green',
    //   radioClass   : 'iradio_flat-green'
    // });
    counter++;

});



$("table.order-list").on("click", ".ibtnDel", function (event) {
    event.preventDefault()
    
    var totalrowcount={{count($prospectproductdetail)}};
    console.log(totalrowcount+counter);
    var counterrowcount=totalrowcount+counter;
    if(counterrowcount>=2)
    {
    $(this).closest("tr").remove();
    counter -= 1
    }
    else
    {
        toastr.error("The Form should contain single Product Row.");

    }
});
});
</script>

<script src="{{ URL::asset('assets/js/pages/toastr.init.js')}}"></script>
@endsection