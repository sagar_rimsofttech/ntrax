@extends('layouts.master')

@section('css')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
        <link href="{{ URL::asset('assets/libs/custombox/custombox.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
        <title>Ntrax - Prospect</title>
        <link href="{{ URL::asset('assets/libs/jquery-toast/jquery-toast.min.css')}}" rel="stylesheet" type="text/css" />
        <style>
            .floatRight {
            float: right;
            margin-left:10px; 
            }
        </style>

@endsection

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Masters</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Prospect</a></li>
                                            {{-- <li class="breadcrumb-item active">Datatables</li> --}}
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Add Quick Prospect</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 
                        <div class="row">
                            <form class="form" action="{{route('prospect.quickstore')}}" method="POST">
                                {{csrf_field()}}
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                Select the Segment: &nbsp;
                                                <div class="radio radio-info form-check-inline">
                                                    <input type="radio" id="inlineRadio1" value="B2B" name="business" checked>
                                                    <label for="inlineRadio1">  B2B </label>
                                                </div>
                                                <div class="radio radio-info form-check-inline">
                                                    <input type="radio" id="inlineRadio2" value="B2C" name="business">
                                                    <label for="inlineRadio2"> B2C </label>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="checkbox checkbox-info mt-1 float-right">
                                                    <select class="form-control "  data-placeholder="Choose Channel Type..." id="channelpartnertype" name="channelpartnertype" style="width:100%" data-style="btn-outline-primary" disabled>
                                                        <option value="">Select Channel partner</option>
                                                        @foreach ($channelpartnertypes as $value)
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                        @endforeach
                                                       
                                                    </select>
                                                </div> 
                                            <div class="mb-2 float-right">
                                                <div class="checkbox checkbox-info mt-0">
                                                    <input id="checkbox4" type="checkbox" >
                                                    <label for="checkbox4">
                                                        
                                                    </label>
                                                </div>
                                            </div>
                                        </div> <!-- end card -->
                                    </div><!-- end col-->
                                    <hr>
                                    <div class="row">&nbsp;</div>
                                    <div class="col-lg-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                               <label for="fname">First Name</label>
                                            <input type="text" class="form-control" value="" name="fname" id="fname" value="{{old('fname') }}">
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="lname">Last Name</label>
                                                <input type="text" class="form-control" name="lname" id="lname" value="{{old('lname') }}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="phone">Phone Number</label>
                                                <input type="text" class="form-control" name="phone" id="phone" value="{{old('phone') }}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control" name="email" id="email" value="{{old('email') }}">
                                             </div>
                                    </div><!-- end col-->
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                               <label for="source">Source</label>
                                               <select class="form-control" name="source" id="source">
                                                <option value="">Select Source</option>
                                                @foreach(config('constant.source') as $key => $value)
                                            <option value="{{$key}}" {{ old('source') == $key ? 'selected' : '' }}>  {{$value}} </option>
                                                @endforeach
                                                </select>
                                            </div>
                                           
                                             <div class="form-group col-md-3">
                                                <label for="addphone">Company Name</label>
                                                <input type="text" class="form-control" name="company_name" id="company_name" value="{{old('company_name') }}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="addphone">SPOC Name</label>
                                                <input type="text" class="form-control" name="spocname" id="spocname" value="{{old('spocname') }}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="source">SPOC Email</label>
                                                <input type="text" class="form-control" name="spocemail" id="spocemail" value="{{old('spocemail') }}">
                                             </div>
                                    </div><!-- end col-->
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-row">
                                          
                                           
                                             <div class="form-group col-md-3">
                                                <label for="addphone">Additional Phone Number</label>
                                                <input type="text" class="form-control" name="addphone" id="addphone" value="{{old('addphone') }}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="country">Country</label>
                                                <select id="country_name" class="form-control country-select2" title="" name="country_name">
                        
                                            </select>
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="state">State</label>
                                                <select id="state_name" class="form-control state-select2"
                                                data-show-subtext="true" name="state_name" data-live-search="true" title="">
                        
                                            </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="city">City</label>
                                                <select id="city_name" class="form-control city-select2"
                                                data-show-subtext="true" name="city_name" data-live-search="true" title="">
                        
                                            </select>
                                             </div>
                                    </div><!-- end col-->
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-row">
                                            
                                          
                                             <div class="form-group col-md-3">
                                                <label for="zone">Zone</label>
                                                <select class="form-control "  data-placeholder="Choose Zone..." id="zone" name="zone" style="width:100%" data-style="btn-outline-primary">
                                                    <option value="">Select Zone</option>
                                                    @foreach ($zones as $value)
                                                <option value="{{$value->id}}" {{ old('zone') == $value->id ? 'selected' : '' }}>{{$value->zonename}}</option>
                                                    @endforeach
                                                   
                                                </select>
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="address">Address</label>
                                                <input type="text" class="form-control" name="address" id="address" value="{{old('address') }}">
                                             </div>
                                             <div class="form-group col-md-3">
                                                <label for="pincode">Pincode</label>
                                                <input type="text" class="form-control" name="pincode" maxlength="6" onkeypress="return isNumber(event)" id="pincode" value="{{old('pincode') }}">
                                             </div>
                                    </div><!-- end col-->
                                    </div>
                                    
                                    <hr>
                                    <div class="row">&nbsp;</div>
                                    <div class="col-lg-12">
                                      
                                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                         </div>
                                    </div>
                                </div>
                            </div> <!-- container -->
                    </form>
                        </div>
                    </div>
                </div>
               
@endsection

@section('script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-XeQiy32hVZnA-Oy9db3bhHRvLpKi3Cc&libraries=places"></script>
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <!-- third party js -->
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <!-- third party js ends -->
        <script src="{{ URL::asset('assets/libs/custombox/custombox.min.js')}}"></script>
        <!-- Datatables init -->
        <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>
       
 <script src="{{ URL::asset('assets/libs/custombox/custombox.min.js')}}"></script>
 <script>
     $(document).ready(function () {
        function initialize() {
        var input = document.getElementById('searchTextField');
        new google.maps.places.Autocomplete(input);
        }

        $('input[type="checkbox"]').click(function() { 
    if ($(this).is(':checked')) {
        //$(this).prop('checked',false);
        $("#channelpartnertype").removeAttr("disabled");
    } else {
         //$(this).prop('checked',true);
         $("#channelpartnertype").attr("disabled", "disabled"); 
    }
});

        google.maps.event.addDomListener(window, 'load', initialize);
      $(".country-select2").select2({
        ajax: {
            url: function (params) {
                return "{{route('prospect-country').'/'}}" + params.term;
            },
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {q: params.term, };
            },
            processResults: function (data, params) {
                return {results: data};
            },
            cache: true
        },
        minimumInputLength: 3,
    });


    $( ".country-select2" ) .change(function () {
        $("#state_name").val('');
        $("#city_name").val('');
        let param_new = $(this).val();
        var param_country = "?country_id="+param_new;
        $(".state-select2").select2({
        ajax: {
            url: function (param) {
                return "{{route('prospect-state').'/'}}" + param.term + param_country;
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        // minimumInputLength: 3,
    });
    });

    $( ".state-select2" ) .change(function () {
        $("#city_name").val('');
        let param_new = $(this).val();
        var param_state = "?state_id="+param_new;
        $(".city-select2").select2({
        ajax: {
            url: function (param) {
                return "{{route('prospect-city').'/'}}" + param.term + param_state;
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        // minimumInputLength: 3,
    });
    });
    })
    
</script>

<script src="{{ URL::asset('assets/js/pages/toastr.init.js')}}"></script>
@endsection