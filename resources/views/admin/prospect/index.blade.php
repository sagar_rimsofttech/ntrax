@extends('layouts.master')

@section('css')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
        <link href="{{ URL::asset('assets/libs/custombox/custombox.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
        <title>Ntrax - Prospect</title>
        <link href="{{ URL::asset('assets/libs/jquery-toast/jquery-toast.min.css')}}" rel="stylesheet" type="text/css" />
        <style>
            .floatRight {
            float: right;
            margin-left:10px;
            }
        </style>

@endsection

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Masters</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Prospect</a></li>
                                            {{-- <li class="breadcrumb-item active">Datatables</li> --}}
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Prospect</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                @php  
                                                $createprospect = ''; 
                                                $createtext = '';
                                                if(!Entrust::can('Create Prospect'))
                                                {
                                                    $createprospect = ''; 
                                                    $createtext = 'You dont Have permission To Create';
                                                }
                                            @endphp
                                                <a href="{{route('prospect.create')}}" class="btn btn-primary mr-1" id="create_record">
                                                    <i class="mdi mdi-plus-circle mr-2"></i> Add Prospect
                                                </a><a href="{{route('prospect.quickcreate')}}" class="btn btn-primary" id="create_record">
                                                    <i class="mdi mdi-plus-circle mr-1"></i> Add Quick Prospect
                                                </a>
                                            </div>
                                           
                                        </div>
                                        <div class="row">&nbsp;</div>
                                        <table  class="table table-striped nowrap fetchdata" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Prospect Code</th>
                                                    <th>Source</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>Created At</th>
                                                    <th>Updated At</th>
                                                   
                                                   
                                                </tr>
                                            </thead>
                                        
                                        <tbody>

                                        </tbody>
                                        </table>
                                        
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->

                        
                    </div> <!-- container -->
                      <!-- Modal -->
@endsection

@section('script')
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <!-- third party js -->
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <!-- third party js ends -->
        <script src="{{ URL::asset('assets/libs/custombox/custombox.min.js')}}"></script>
        <!-- Datatables init -->
        <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

       
       <script>
        $(document).ready(function () {
                 $(".country-select2").select2({
                   ajax: {
                       url: function (params) {
                           return "{{route('search.country').'/'}}" + params.term;
                       },
                       dataType: 'json',
                       delay: 250,
                       data: function (params) {
                           return {q: params.term, };
                       },
                       processResults: function (data, params) {
                           return {results: data};
                       },
                       cache: true
                   },
                   minimumInputLength: 3,
               });
           
           
               $( ".country-select2" ) .change(function () {
                   $("#state_name").val('');
                   $("#city_name").val('');
                   let param_new = $(this).val();
                   var param_country = "?country_id="+param_new;
                   $(".state-select2").select2({
                   ajax: {
                       url: function (param) {
                           return "{{route('search.state').'/'}}" + param.term + param_country;
                       },
                       dataType: 'json',
                       delay: 250,
                       data: function (param) {
                           return {q: param.term, };
                       },
                       processResults: function (data, param) {
                           return {results: data};
                       },
                       cache: true
                   },
                   // minimumInputLength: 3,
               });
               });
           
               $( ".state-select2" ) .change(function () {
                   $("#city_name").val('');
                   let param_new = $(this).val();
                   var param_state = "?state_id="+param_new;
                   $(".city-select2").select2({
                   ajax: {
                       url: function (param) {
                           return "{{route('search.city').'/'}}" + param.term + param_state;
                       },
                       dataType: 'json',
                       delay: 250,
                       data: function (param) {
                           return {q: param.term, };
                       },
                       processResults: function (data, param) {
                           return {results: data};
                       },
                       cache: true
                   },
                   // minimumInputLength: 3,
               });
               });
           });
        </script>
         <script>
            $(document).ready(function(){
                var datatable = '';
         var addprospectcheck = "{{ Entrust::can('Create Prospect') ? 'true' : 'false' }}";
         datatable = $('.fetchdata').DataTable({
          processing: true,
          serverSide: true,
          bDestroy: true,
          scrollX: true,
          ajax:{
           url: "{{ route('prospect.index') }}",
          },
        //   data: data,
          columns:[
            {
                data:'DT_RowIndex',
                name:'DT_RowIndex'

            },
            {
                data : 'prospect_code',
                name : 'prospect_code'
           },
            {
                data : 'source',
                name : 'source'
           },
            {
                data : 'first_name',
                name : 'first_name'
           },
            {
                data : 'last_name',
                name : 'last_name'
           },
            {
                data : 'phone',
                name : 'phone'
           },
            {
                data : 'spoc_email',
                name : 'spoc_email'
           },
           
           {
                data : 'created_at',
                name : 'created_at'
           },
           {
                data : 'updated_at',
                name : 'updated_at'
           }
           
          ],
           
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
         });
            function unauthorized()
            {
                $.NotificationApp.send("You Are not unauthorized to do this action.", "Yes! check this <a href='https://github.com/kamranahmedse/jquery-toast-plugin/commits/master'>To Request For Approval</a>.",'top-right', '#bf441d', 'error', 8000, 100, 'fade');
            }
        });
        </script>
      <script src="{{ URL::asset('assets/libs/jquery-toast/jquery-toast.min.js')}}"></script>

      <!-- toastr init js-->
      <script src="{{ URL::asset('assets/js/pages/toastr.init.js')}}"></script>
 <script src="{{ URL::asset('assets/libs/custombox/custombox.min.js')}}"></script>

@endsection