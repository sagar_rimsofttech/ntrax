<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProspectProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospect_product_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('prospect_id');
            $table->integer('product_id');
            $table->integer('subproduct_id');
            $table->integer('subsubproduct_id');
            $table->float('price');
            $table->float('margin');
            $table->float('discount');
            $table->float('total_price');
            $table->float('tax');
            $table->float('total_net_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospect_product_details');
    }
}
