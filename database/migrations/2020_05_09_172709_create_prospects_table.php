<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prospect_code');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->biginteger('phone');
            $table->string('email');
            $table->integer('source');
            $table->integer('customer_type')->nullable();
            $table->string('company_name');
            $table->string('spoc_name')->nullable();
            $table->string('spoc_email')->nullable();
            $table->biginteger('addit_phone')->nullable();
            $table->integer('country')->nullable();
            $table->integer('state')->nullable();
            $table->integer('city')->nullable();
            $table->integer('zone')->nullable();
            $table->integer('pincode')->nullable();
            $table->text('address')->nullable();
            $table->text('google_address')->nullable();
            $table->string('segment')->nullable();
            $table->integer('channel_partner_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospects');
    }
}
