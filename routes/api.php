<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => 'check-json','cors'], function () {
    // Route::post('ledger/login', 'Api\HomeController@index');
    Route::get('getprospect','Api\SocialMediaRequest\ProspectController@index');
    Route::post('store-socialmedia-prospect', 'Api\SocialMediaRequest\ProspectController@savesocialmediaprospect');
   
   
});

