<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Prospect\IndexProspectRequest;
use App\Http\Requests\Prospect\StoreProspectRequest;
use App\Http\Requests\Prospect\StoreQuickProspectRequest;
use App\Models\ChannelPartnerType;
use App\Models\Prospect;
use App\Ntrax\Repositories\ChannelPartnerType\ChannelPartnerTypeInterface;
use App\Ntrax\Repositories\Prospect\ProspectInterface;
use App\Ntrax\Repositories\Zone\ZoneInterface;
use DataTables;
use Illuminate\Support\Facades\DB;
use Validator;
use Zizaco\Entrust\EntrustFacade as Entrust;

class ProspectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $prospect;
    private $channelpartnertype;
    private $zone;
      public function __construct(ProspectInterface $prospect,ChannelPartnerTypeInterface $channelpartnertype,ZoneInterface $zone)
    {
        $this->prospect = $prospect;
        $this->channelpartnertype = $channelpartnertype;
        $this->zone=$zone;
      
    }
    
    public function index(IndexProspectRequest $request)
    {
        if ($request->ajax()) {
            return   $prospects = $this->prospect->getallprospects($request);
          }
       return view('admin.prospect.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $zones = $this->zone->getZone();
        $channelpartnertypes = $this->channelpartnertype->searchChannelPartnerType();
        return view('admin.prospect.create',compact('channelpartnertypes','zones'));
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProspectRequest $request)
    {
        
        DB::beginTransaction();
            try{
                DB::commit();
                $saveprospect= $this->prospect->storeprospect($request);
                $notification = array(
                    'message' => 'Prospect created successfully!',
                    'alert-type' => 'success'
                );
                return redirect()->route('prospect.index')->with($notification);
            }  catch (\Exception $e) {
                DB::rollBack();
                logger($e->getMessage());
                $notification = array(
                    'message' => 'Prospect not added!',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }

            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $prospect = $this->prospect->editprospect($id);
        $prospectdetail = $prospect['prospect'];
        $prospectproductdetail = $prospect['prospectproduct'];
        $zones = $this->zone->getZone();
        $channelpartnertypes = $this->channelpartnertype->searchChannelPartnerType();
        return view('admin.prospect.edit',compact('prospectdetail','channelpartnertypes','zones','prospectproductdetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all(),$id);
        

        DB::beginTransaction();
        try{
            DB::commit();
            $prospect = $this->prospect->updateProspects($request,$id);
            $notification = array(
                'message' => 'Prospect Updated successfully!',
                'alert-type' => 'success'
            );
            return redirect()->route('prospect.index')->with($notification);
        }  catch (\Exception $e) {
            DB::rollBack();
            logger($e->getMessage());
            dd($e->getMessage());
            $notification = array(
                'message' => 'Prospect not Updated!',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function searchCountry($keyword=null)
    {
        $countries = $this->prospect->searchCountry($keyword);
        $countryArray=[];
        foreach ($countries as $countrykey=>$country){
            $countryArray[$countrykey]['id']=$country->id;
            $countryArray[$countrykey]['text']=$country->country_name;
        }
        return $countryArray;
    }

    public function searchState($keyword=null,Request $request)
    {
        $states = $this->prospect->searchState($keyword,$request);
        $stateArray=[];
        foreach ($states as $stateykey=>$state){
            $stateArray[$stateykey]['id']=$state->id;
            $stateArray[$stateykey]['text']=$state->state_name;
        }
        return $stateArray;
    }

    public function searchCity($keyword=null,Request $request)
    {
        $cities = $this->prospect->searchCity($keyword,$request);
        $cityArray=[];
        foreach ($cities as $citykey=>$city){
            $cityArray[$citykey]['id']=$city->id;
            $cityArray[$citykey]['text']=$city->city_name;
        }
        return $cityArray;
    }
    
    public function quickcreate()
    {
        $zones = $this->zone->getZone();
        $channelpartnertypes = $this->channelpartnertype->searchChannelPartnerType();
        return view('admin.prospect.quickcreate',compact('channelpartnertypes','zones'));
    }

    public function quickstore(StoreQuickProspectRequest $request)
    {
        
        DB::beginTransaction();
            try{
                DB::commit();
                $saveprospect= $this->prospect->storequickprospect($request);
                $notification = array(
                    'message' => 'Prospect created successfully!',
                    'alert-type' => 'success'
                );
                return redirect()->route('prospect.index')->with($notification);
            }  catch (\Exception $e) {
                DB::rollBack();
                logger($e->getMessage());
                //dd($e->getMessage());
                $notification = array(
                    'message' => 'Prospect not added!',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }

            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
}
