<?php

namespace App\Http\Controllers\Api\SocialMediaRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SocialMediaProspect\StoreSocialMediaProspectRequest;
use App\Http\Requests\Prospect\StoreQuickProspectRequest;
use App\Ntrax\Repositories\Prospect\ProspectInterface;
use Illuminate\Support\Facades\DB;

class ProspectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $prospect;
    
      public function __construct(ProspectInterface $prospect)
    {
        $this->prospect = $prospect;
      
    }
    public function index()
    {
        dd(1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function savesocialmediaprospect(StoreSocialMediaProspectRequest $request)
    {
        DB::beginTransaction();
            try{
                
                DB::commit();
                $saveprospect= $this->prospect->storesocialmediaprospect($request);
                $notification = array(
                    'message' => 'Prospect created successfully!',
                    'alert-type' => 'success'
                );
                return response()->json([
                    'message' => trans('success.PROSPECT_001'),
                ], 200);
    
            }  catch (\Exception $e) {
                dd($e->getMessage());
                DB::rollBack();
                logger($e->getMessage());

               
                return response()->json(['message' => trans('errors.ERROR_001')],400);

            }

            
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
