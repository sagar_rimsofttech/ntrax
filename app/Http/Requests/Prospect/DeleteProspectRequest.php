<?php

namespace App\Http\Requests\Prospect;

use App\Models\Prospect;
use App\Policies\ProspectPolicy;
use Illuminate\Foundation\Http\FormRequest;

class DeleteProspectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // dd($this->user()->can('Delete Zone'));
        return policy(Prospect::class)->delete($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
        // dd(1);
        // return response()->json(['success' => 'Unsuccessfully updated.']);
    }
}
