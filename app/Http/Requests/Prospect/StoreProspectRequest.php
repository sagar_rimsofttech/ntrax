<?php

namespace App\Http\Requests\Prospect;

use App\Models\Prospect;
use App\Policies\ProspectPolicy;
use Illuminate\Foundation\Http\FormRequest;

class StoreProspectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // dd($this->user()->can('List Zone'));
        return policy(Prospect::class)->create($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'business'   => 'required',
        // 'channelpartnertype'   => 'required',
        'fname'    =>  'required|min:3|max:35|regex:/^([a-zA-Z\' ])*$/',
        'lname'    =>  'required|min:3|max:35|regex:/^([a-zA-Z\' ])*$/',
        'phone'  => 'required|numeric|regex:/^[1-9][0-9]{6,14}/|min:7',
        'email'       =>  'required|email',
        'source'   => 'required',
        'customertype'   => 'required',
        'company_name'    =>  'required|min:3|max:35|regex:/^([a-zA-Z\' ])*$/',
        'spocname'    =>  'required|min:3|max:35|regex:/^([a-zA-Z\' ])*$/',
        'spocemail'       =>  'required|email',
        'addphone' => 'required|numeric|regex:/^[1-9][0-9]{6,14}/',
        'country_name'   => 'required',
        'state_name'   => 'required',
        'city_name'   => 'required',
        'zone'   => 'required',
        'address'    =>  'required|min:3|max:35|regex:/^([a-zA-Z\' ])*$/',
        'pincode'    =>  'required',
        'googleaddress'    =>  'required',
        ];
    }

    public function messages()
    {
        return [
            'business.required' => trans('errors.PROSPECT_101'),
            // 'channelpartnertype.required' => trans('errors.PROSPECT_102'),
            'fname.required' => trans('errors.PROSPECT_103'),
            'fname.min' => trans('errors.PROSPECT_104'),
            'fname.regex' => trans('errors.PROSPECT_105'),
            'lname.required' => trans('errors.PROSPECT_106'),
            'lname.min' => trans('errors.PROSPECT_107'),
            'lname.regex' => trans('errors.PROSPECT_108'),
            'phone.required' => trans('errors.PROSPECT_109'),
            'phone.numeric' => trans('errors.PROSPECT_110'),
            'phone.regex' => trans('errors.PROSPECT_111'),
            'email.required' => trans('errors.PROSPECT_112'),
            'source.required' => trans('errors.PROSPECT_113'),
            'customertype.required' => trans('errors.PROSPECT_114'),
            'company_name.required' => trans('errors.PROSPECT_115'),
            'company_name.min' => trans('errors.PROSPECT_116'),
            'company_name.regex' => trans('errors.PROSPECT_117'),
            'spocname.required' => trans('errors.PROSPECT_118'),
            'spocname.min' => trans('errors.PROSPECT_119'),
            'spocname.regex' => trans('errors.PROSPECT_120'),
            'spocemail.required' => trans('errors.PROSPECT_121'),
            'addphone.required'  => trans('errors.PROSPECT_122'),
            'addphone.numeric' => trans('errors.PROSPECT_123'),
            'country_name.required' => trans('errors.PROSPECT_124'),
            'state_name.required' => trans('errors.PROSPECT_125'),
            'city_name.required' => trans('errors.PROSPECT_126'),
            'zone.required' => trans('errors.PROSPECT_127'),
            'address.required' => trans('errors.PROSPECT_128'),
            'pincode.required' => trans('errors.PROSPECT_129'),
            'googleaddress.required' => trans('errors.PROSPECT_130'),
           
        ];
           
    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
        // dd(1);
        // return response()->json(['success' => 'Unsuccessfully updated.']);
    }
}
