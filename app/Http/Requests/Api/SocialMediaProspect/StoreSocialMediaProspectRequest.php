<?php

namespace App\Http\Requests\Api\SocialMediaProspect;

use App\Models\Prospect;
use App\Policies\ProspectPolicy;
use Illuminate\Foundation\Http\FormRequest;

class StoreSocialMediaProspectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // dd($this->user()->can('List Zone'));
        //return policy(Prospect::class)->create($this->user());
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
       
        'fname'    =>  'required|min:3|max:35|regex:/^([a-zA-Z\' ])*$/',
        'lname'    =>  'required|min:3|max:35|regex:/^([a-zA-Z\' ])*$/',
        'phone'  => 'required|numeric|regex:/^[1-9][0-9]{6,14}/|min:7',
        'email'       =>  'required|email',
        'source'   => 'required|in:0,1,2,3,4,5',
        'company_name'    =>  'required|min:3|max:35|regex:/^([a-zA-Z\' ])*$/',
        
       
        ];
    }

    public function messages()
    {
        return [
            
            'fname.required' => trans('errors.PROSPECT_103'),
            'fname.min' => trans('errors.PROSPECT_104'),
            'fname.regex' => trans('errors.PROSPECT_105'),
            'lname.required' => trans('errors.PROSPECT_106'),
            'lname.min' => trans('errors.PROSPECT_107'),
            'lname.regex' => trans('errors.PROSPECT_108'),
            'phone.required' => trans('errors.PROSPECT_109'),
            'phone.numeric' => trans('errors.PROSPECT_110'),
            'phone.regex' => trans('errors.PROSPECT_111'),
            'email.required' => trans('errors.PROSPECT_112'),
            'source.required' => trans('errors.PROSPECT_113'),
            'company_name.required' => trans('errors.PROSPECT_115'),
            'company_name.min' => trans('errors.PROSPECT_116'),
            'company_name.regex' => trans('errors.PROSPECT_117'),
        ];
           
    }

    
}
