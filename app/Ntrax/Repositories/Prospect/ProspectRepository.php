<?php

namespace App\Ntrax\Repositories\Prospect;

use App\Models\City;
use App\Models\Country;
use App\Models\Prospect;
use App\Models\Product;
use App\Models\ProspectProductDetail;
use App\Models\State;
use App\Models\SubProduct;
use App\Models\SubSubProduct;
use App\Ntrax\Repositories\Prospect\ProspectInterface;
use App\Role;
use DataTables;

class ProspectRepository implements ProspectInterface
{
    private $prospect;
    private $prospectproduct;
    public function __construct(Prospect $prospect,ProspectProductDetail $prospectproduct){
        $this->prospect = $prospect;
        $this->prospectproduct = $prospectproduct;
      
    }

   /* 

    public function updateprospect($request)
    {
        $subsubproduct = $this->subsubproduct->find($request->hidden_id);
        $subsubproduct->name = $request->get('name');
        $subsubproduct->subproduct_id = $request->get('subproduct_name');
        $subsubproduct->rate = $request->get('rate');
        $subsubproduct->margin = $request->get('margin');
        $subsubproduct->max_discount = $request->get('discount');
        $subsubproduct->save();
        return $subsubproduct;
    }

     public function geteditdata($id)
     {
        $subsubproduct = $this->subsubproduct->findOrFail($id);
       // dd($subsubproduct);
        $subproductname = $this->subproduct->geteditdata($subsubproduct->subproduct_id);
        $subsubproduct->subproductname = $subproductname->name;
        return $subsubproduct;

    }

    public function destroyprospect($id)
    {
        $data = $this->subsubproduct::findOrFail($id);
        $data->delete();
        return $data;
    }*/
    public function getallprospects($request)
    {
        $data = $this->prospect::latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('prospect_code', function ($data) {
                    return '<a href="' . route('prospect.edit', $data->id) .'">'.$data->prospect_code.'</a>'; 
                })
                ->editColumn('source', function ($data) {
                   // $sourcename = $this->prospect->geteditdata($data->source);
                    return config('constant.source')[$data->source];
                })
                ->editColumn('created_at', function ($data) {
                    return $data->created_at->diffForHumans();
                })
                ->editColumn('updated_at', function ($data) {
                    return $data->updated_at->diffForHumans();
                })
                             
               
                ->rawColumns(['prospect_code'])
                ->make(true);

    }

    public function editprospect($id)
    {
       $prospect = $this->prospect->findOrFail($id);
       $prospectproducts = $this->prospectproduct::where('prospect_id',$prospect->id)->get();
       $subcountries_name =Country::where('id',$prospect->country)->select('country_name')->first();
       $cities_name =City::where('id',$prospect->city)->select('city_name')->first();
       $states_name =State::where('id',$prospect->state)->select('state_name')->first();
       $prospect->city_name = $cities_name->city_name??'';
       $prospect->state_name = $states_name->state_name??'';
       $prospect->country_name = $subcountries_name->country_name??'';

    //    foreach($prospectproducts as $key => $value)
    //    {
    //    $product_name =Product::where('id',$value->product_id)->select('name')->first();
    //    $subproduct_name =SubProduct::where('id',$value->subproduct_id)->select('name')->first();
    //    $subsubproduct_name =SubSubProduct::where('id',$value->subsubproduct_id)->select('name')->first();
    //    $prospectproducts->product_name = $product_name->name;
    //    $prospect->subproduct_name = $states_name->name;
    //    $prospect->subsubproduct_name = $subcountries_name->name;
    //    }
       //dd($prospect);

       return ['prospect' => $prospect,'prospectproduct' => $prospectproducts];

   }
    public function storeprospect($request)
    {
       // dd($request->all());
        $prospect = $this->prospect;
        $prospect->first_name = $request->get('fname');
        $prospect->middle_name = $request->get('lname');
        $prospect->last_name = $request->get('lname');
        $prospect->phone = $request->get('phone');
        $prospect->email = $request->get('email');
        $prospect->source = $request->get('source');
        $prospect->customer_type = $request->get('customertype');
        $prospect->company_name = $request->get('company_name');
        $prospect->spoc_name = $request->get('spocname');
        $prospect->spoc_email = $request->get('spocemail');
        $prospect->addit_phone = $request->get('addphone');
        $prospect->country = $request->get('country_name');
        $prospect->state = $request->get('state_name');
        $prospect->city = $request->get('city_name');
        $prospect->zone = $request->get('zone');
        $prospect->pincode = $request->get('pincode');
        $prospect->address = $request->get('address');
        $prospect->google_address = $request->get('googleaddress');
        $prospect->segment = $request->get('business');
        $prospect->channel_partner_id = $request->get('channelpartnertype')??'0';
        $prospect->save();
        $product_abc= $request->product;
        foreach($product_abc as $key=>$product){
            $prospectproduct = new ProspectProductDetail();
            $prospectproduct->prospect_id = $prospect->id;
            $prospectproduct->product_id = $request->product[$key];
            $prospectproduct->subproduct_id = $request->subproduct[$key];
            $prospectproduct->subsubproduct_id = $request->subsubproduct[$key];
            $prospectproduct->price = $request->price[$key];
            $prospectproduct->margin = $request->margin[$key];
            $prospectproduct->discount = $request->discount[$key];
            $prospectproduct->total_price = $request->totalprice[$key];
            $prospectproduct->tax = $request->tax[$key];
            $prospectproduct->total_net_price = $request->totalnetprice[$key];
           $prospectproduct->save();
        }
        return $prospect;
    }

    public function storequickprospect($request)
    {
       
        $prospect = $this->prospect;
        $prospect->first_name = $request->get('fname');
        $prospect->middle_name = $request->get('lname');
        $prospect->last_name = $request->get('lname');
        $prospect->phone = $request->get('phone');
        $prospect->email = $request->get('email');
        $prospect->source = $request->get('source');
        $prospect->company_name = $request->get('company_name');
        $prospect->spoc_name = $request->get('spocname');
        $prospect->spoc_email = $request->get('spocemail');
        $prospect->addit_phone = $request->get('addphone');
        $prospect->country = $request->get('country_name');
        $prospect->state = $request->get('state_name');
        $prospect->city = $request->get('city_name');
        $prospect->zone = $request->get('zone');
        $prospect->pincode = $request->get('pincode');
        $prospect->address = $request->get('address');
        $prospect->segment = $request->get('business');
        $prospect->channel_partner_id = $request->get('channelpartnertype')??'0';
        //dd($prospect);
        $prospect->save();
       
        return $prospect;
    }

    public function storesocialmediaprospect($request)
    {
       
        $prospect = $this->prospect;
        $prospect->first_name = $request->get('fname');
        $prospect->last_name = $request->get('lname');
        $prospect->phone = $request->get('phone');
        $prospect->email = $request->get('email');
        $prospect->source = $request->get('source');
        $prospect->company_name = $request->get('company_name');
        $prospect->save();
       
        return $prospect;
    }

    public function searchCountry($keyword = null)
    {
        return  $countries_name =Country::where('country_name', 'like', '%'.$keyword.'%')->select('id', 'country_name')->limit(10)->get();
    }

    public function searchState($keyword = null,$request)
    {
        return  $states_name =State::where('country_id',$request->country_id)->where('state_name', 'like', '%'.$request->q.'%')->select('id', 'state_name')->get();
    }

    public function searchCity($keyword = null,$request)
    {
        return  $states_name =City::where('state_id',$request->state_id)->where('city_name', 'like', '%'.$request->q.'%')->select('id', 'city_name')->get();
    }
    public function updateProspects($request,$id)
    {
      
        $prospect = $this->prospect->findOrFail($id);
        $prospect->first_name = $request->get('fname');
        $prospect->middle_name = $request->get('lname');
        $prospect->last_name = $request->get('lname');
        $prospect->phone = $request->get('phone');
        $prospect->email = $request->get('email');
        $prospect->source = $request->get('source');
        $prospect->company_name = $request->get('company_name');
        $prospect->spoc_name = $request->get('spocname');
        $prospect->spoc_email = $request->get('spocemail');
        $prospect->addit_phone = $request->get('addphone');
        $prospect->country = $request->get('country_name');
        $prospect->state = $request->get('state_name');
        $prospect->city = $request->get('city_name');
        $prospect->zone = $request->get('zone');
        $prospect->pincode = $request->get('pincode');
        $prospect->address = $request->get('address');
        $prospect->segment = $request->get('business');
        $prospect->channel_partner_id = $request->get('channelpartnertype')??'0';
        $prospect->google_address = $request->get('googleaddress');
        $prospect->save();
        $prospectproducts = $this->prospectproduct::where('prospect_id',$id)->get('id');
        foreach ($prospectproducts as $prospectproduct) {
        $prospectproduct->delete();
        }
        $prospect_products= $request->product;
        foreach($prospect_products as $key=>$product){
            $prospectproduct = new ProspectProductDetail();
            $prospectproduct->prospect_id = $id;
            $prospectproduct->product_id = $request->product[$key];
            $prospectproduct->subproduct_id = $request->subproduct[$key];
            $prospectproduct->subsubproduct_id = $request->subsubproduct[$key];
            $prospectproduct->price = $request->price[$key];
            $prospectproduct->margin = $request->margin[$key];
            $prospectproduct->discount = $request->discount[$key];
            $prospectproduct->total_price = $request->totalprice[$key];
            $prospectproduct->tax = $request->tax[$key];
            $prospectproduct->total_net_price = $request->totalnetprice[$key];
           $prospectproduct->save();
        }
        return $prospect;
    }
    

}
