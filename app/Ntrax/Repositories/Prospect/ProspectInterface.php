<?php

namespace App\Ntrax\Repositories\Prospect;

interface ProspectInterface
{
   /* 
     public function geteditdata($id);
     public function storeprospect($request);
     public function updateprospect($request);
     public function destroyprospect($id);*/
     public function getallprospects($request);
     public function editprospect($id);
     public function storeprospect($request);
     public function storesocialmediaprospect($request);
     public function storequickprospect($request);
     public function searchCountry($keyword = null);
     public function searchState($keyword = null,$request);
     public function searchCity($keyword = null,$request);
     public function updateProspects($request,$id);
     

}
