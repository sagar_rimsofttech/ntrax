<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProspectProductDetail extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function subproduct()
    {
        return $this->belongsTo(SubProduct::class);
    }
    public function subsubproduct()
    {
        return $this->belongsTo(SubSubProduct::class);
    }

    
}
